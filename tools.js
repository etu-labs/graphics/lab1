document.getElementById('scaleRange').addEventListener('input', function (e) {
    params.scale = parseFloat(e.target.value)
    loop()
})

document.getElementById('offsetX').addEventListener('input', function (e) {
    params.offsetX = parseFloat(e.target.value)
    loop()
})

document.getElementById('offsetY').addEventListener('input', function (e) {
    params.offsetY = parseFloat(e.target.value)
    loop()
})

document.getElementById('angle').addEventListener('input', function (e) {
    params.angle = parseFloat(e.target.value)
    loop()
})

document.getElementById('color').addEventListener('input', function (e) {
    params.color = normalize(hexToRgb(e.target.value))
    loop()
})
