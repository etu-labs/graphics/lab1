function loadShader(shader) {
    return fetch(`/shaders/${shader}.glsl`, { mode: 'cors' }).then(file => file.text())
}
