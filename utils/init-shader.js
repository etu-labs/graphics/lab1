function initShader(gl, shaderSource, type) {
    const shader = gl.createShader(type)
    gl.shaderSource(shader, shaderSource)
    gl.compileShader(shader)
    return shader
}
