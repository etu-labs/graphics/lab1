const width = 1000
const height = 700
const backgroundColor = [0.75, 0.8, 0.85]

function clearCanvas() {
    gl.clearColor(...backgroundColor, 1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.viewport(0, 0, width, height);
}

function prepareCanvas(canvas) {
    canvas.style.width = `${width}px`
    canvas.style.height = `${height}px`
}
