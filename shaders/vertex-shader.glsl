attribute vec3 coordinates;
uniform vec4 translation;
uniform vec3 scale;
uniform vec2 rotate;

void main(void) {
    gl_Position = vec4(
    coordinates.x * rotate.x - coordinates.y * rotate.y,
    coordinates.x * rotate.y + coordinates.y * rotate.x,
    coordinates.z,
    1.0) * vec4(scale, 1.0) + translation;
}
