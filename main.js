var gl;
const shaders = {
    fragment: null,
    vertex: null,
    program: null
}
const params = {
    scale: 1,
    offsetX: 0,
    offsetY: 0,
    angle: 0,
    color: [0.0, 0.0, 0.0]
}

async function init() {
    const canvas = document.getElementById('main-canvas')
    prepareCanvas(canvas)
    initWebGl(canvas)
    await initShaders()
    loop()
}

function initWebGl(canvas) {
    gl = canvas.getContext('webgl')

    if (!gl) {
        gl = canvas.getContext('experimantal-webgl')
    }
}

async function initShaders() {
    const vertexSource = await loadShader('vertex-shader')
    shaders.vertex = initShader(gl, vertexSource, gl.VERTEX_SHADER)
    const fragmentSource = await loadShader('fragment-shader')
    shaders.fragment = initShader(gl, fragmentSource, gl.FRAGMENT_SHADER)
    shaders.program = gl.createProgram()
    gl.attachShader(shaders.program, shaders.vertex)
    gl.attachShader(shaders.program, shaders.fragment)
    gl.linkProgram(shaders.program)
    gl.useProgram(shaders.program)
}

function loop() {
    clearCanvas()
    drawTriangle([
        { x: 0.0, y: 0.5, z: 0.0 },
        { x: -0.5, y: 0.0, z: 0.0 },
        { x: 0.5, y: 0.0, z: 0.0 },
    ])
}

function drawTriangle(vertex) {
    const triangle = new Triangle(vertex)
    triangle.setScale(params.scale)
    triangle.setColor(...params.color)
    triangle.setTranslate(params.offsetX, params.offsetY)
    triangle.setRotate(params.angle)
    triangle.draw()
}


