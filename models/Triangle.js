class Triangle {
    #vertex = [] // набор вершин
    #indices = [0, 1, 2] // индексы вершин

    constructor(initialVertex) {
        // инициализирующий набор вершин
        this.#vertex = initialVertex
        const vertexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
        gl.bufferData(gl.ARRAY_BUFFER, this.getVertex(), gl.STATIC_DRAW);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        const indexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, this.getIndices(), gl.STATIC_DRAW);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
        const coordinates = gl.getAttribLocation(shaders.program, "coordinates");
        gl.vertexAttribPointer(coordinates, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(coordinates);
    }

    // получение вершин в виде массива Float32
    getVertex() {
        return new Float32Array(this.#vertex.map(v => [v.x, v.y, v.z]).flat())
    }

    // получение индексов в виде массива Uint16
    getIndices() {
        return new Uint16Array(this.#indices)
    }

    // установка масштабирования
    setScale(value) {
        const scale = gl.getUniformLocation(shaders.program, 'scale')
        gl.uniform3f(scale, value, value, 1.0)
    }

    // установка сдвига
    setTranslate(x = 0, y = 0, z = 0) {
        const translate = gl.getUniformLocation(shaders.program, 'translation')
        gl.uniform4f(translate, x, y, 0.0, 0.0)
    }

    // установка вращения
    setRotate(angle) {
        const radian = Math.PI * angle / 180.0
        const sin = Math.sin(radian)
        const cos = Math.cos(radian)
        const rotate = gl.getUniformLocation(shaders.program, 'rotate')
        gl.uniform2f(rotate, cos, sin)
    }

    // установка цвета
    setColor(r, g, b) {
        const colorUniform = gl.getUniformLocation(shaders.program, 'color')
        gl.uniform3f(colorUniform, r, g, b)
    }

    // функция отрисовки
    draw() {
        gl.drawArrays(gl.TRIANGLES, 0, 3);
    }
}
